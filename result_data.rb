require 'mysql'

class ResultData
  Raw = Struct.new(
      :rRaceCode,  :rKeibajoCode,  :rDate,
      :rRaceNo,    :rTorokusu,
      :rTan_U,     :rTan_C,
      :rSanpu_u1, :rSanpu_u2,  :rSanpu_u3, :rSanpu_C,
      :rSantan_u1,:rSantan_u2, :rSantan_u3,:rSantan_C
  )

  def initialize(order)
    @db = []
    case order
      when 0 then
        my = Mysql::new("210.150.110.214","marusasu_test","marusasu","marusasu_jra")
        a = my.query("select\n
          RACE_CODE,            KEIBAJO_CODE,               KAISAI_NENGAPPI,       RACE_BANGO, TOROKU_TOSU,\n
          TANSHO1_UMABAN,       TANSHO1_HARAIMODOSHIKIN,    SANRENPUKU1_KUMIBAN1,  SANRENPUKU1_KUMIBAN2,\n
          SANRENPUKU1_KUMIBAN3, SANRENPUKU1_HARAIMODOSHIKIN,SANRENTAN1_KUMIBAN1,  SANRENTAN1_KUMIBAN2,\n
          SANRENTAN1_KUMIBAN3,  SANRENTAN1_HARAIMODOSHIKIN \n
          from JVD_HARAIMODOSHI Order by RACE_CODE ASC").each do |rc,kc,date,rn,tt,tan_u,tan_h,sanpu_u1,sanpu_u2,sanpu_u3,sanpu_h,
                                                santan_u1,santan_u2,santan_u3,santan_h|
          puts "#{rc},#{kc},#{date},#{rn},#{tt},#{tan_u},#{tan_h},#{sanpu_u1},#{sanpu_u2},#{sanpu_u3},#{sanpu_h},#{santan_u1},#{santan_u2},#{santan_u3},#{santan_h}"
          @db << Raw.new(rc,kc,date,rn,tt,tan_u,tan_h,sanpu_u1,sanpu_u2,sanpu_u3,sanpu_h,santan_u1,santan_u2,santan_u3,santan_h)
        end
        my.close
        serialize
      when 1 then
        @b = File.open("out.txt","rb")
        @db = Marshal.load(@b)
        #puts @db
      else
        puts "error"
        return @db
    end
  end

  def serialize
    @d = Marshal.dump(@db)
    File.open("out.txt","wb") do |file|
      file.write @d
    end
  end
  def getSearchOrder(order)
    @s = []
    @db.each do |db|
      if db[order[0]] == order[1]
        if order[2] != nil
          if db[order[2]] == order[3]
            #puts db[order[0]]
            @s << Raw.new(db)
          end
        end
      end
    end
    puts @s[0][:rRaceCode]
    return @s
  end
end
#ResultData.new(0)