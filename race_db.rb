require 'mysql'

class RaceDB
  def initialize
    @kubun = nil
    @getRaceDataString =
    "SELECT
      JVD_RACE_SHOSAI.RACE_CODE,
      JVD_RACE_SHOSAI.KAISAI_NENGAPPI,
      JVD_RACE_SHOSAI.KEIBAJO_CODE,
      JVD_RACE_SHOSAI.RACE_BANGO,
      JVD_RACE_SHOSAI.KYORI,
      JVD_RACE_SHOSAI.TRACK_CODE,
      JVD_UMAGOTO_RACE_JOHO.UMABAN,
      JVD_UMAGOTO_RACE_JOHO.KETTO_TOROKU_BANGO,
      JVD_UMAGOTO_RACE_JOHO.BAMEI
    FROM marusasu_jra.JVD_RACE_SHOSAI
    LEFT JOIN marusasu_jra.JVD_UMAGOTO_RACE_JOHO
    ON JVD_RACE_SHOSAI.RACE_CODE = JVD_UMAGOTO_RACE_JOHO.RACE_CODE
    WHERE JVD_RACE_SHOSAI.DATA_KUBUN = 'PARAM_KUBUN'
     AND (JVD_RACE_SHOSAI.KYOSO_SHUBETSU_CODE = 13
      OR JVD_RACE_SHOSAI.KYOSO_SHUBETSU_CODE = 14)
     AND (JVD_RACE_SHOSAI.KYOSO_JOKEN_CODE_SAIJAKUNEN = '016'
       OR JVD_RACE_SHOSAI.KYOSO_JOKEN_CODE_SAIJAKUNEN = '999')
     AND JVD_RACE_SHOSAI.KAISAI_NENGAPPI PARAM_DATE
    ORDER BY JVD_RACE_SHOSAI.KAISAI_NENGAPPI;"

    @getUmaDataString =
    "SELECT
     JVD_UMAGOTO_RACE_JOHO.RACE_CODE,
     JVD_UMAGOTO_RACE_JOHO.KAISAI_NENGAPPI,
     JVD_UMAGOTO_RACE_JOHO.KEIBAJO_CODE,
     MIN(JVD_UMAGOTO_RACE_JOHO.SOHA_TIME),
     JVD_RACE_SHOSAI.KYORI,
     JVD_RACE_SHOSAI.TRACK_CODE,
     JVD_UMAGOTO_RACE_JOHO.TANSHO_ODDS
    FROM marusasu_jra.JVD_UMAGOTO_RACE_JOHO
     LEFT JOIN marusasu_jra.JVD_RACE_SHOSAI
     ON JVD_UMAGOTO_RACE_JOHO.RACE_CODE = JVD_RACE_SHOSAI.RACE_CODE
    WHERE JVD_UMAGOTO_RACE_JOHO.DATA_KUBUN = 'PARAM_KUBUN'
      AND KETTO_TOROKU_BANGO = PARAM_KETTOBANGO
    GROUP BY JVD_UMAGOTO_RACE_JOHO.KEIBAJO_CODE,
             JVD_RACE_SHOSAI.KYORI;"

    @getTansoOddsString =
    "SELECT UMABAN, TANSHO_ODDS
    FROM  marusasu_jra.JVD_TANPUKU_ODDS
    WHERE DATA_KUBUN = 1 AND RACE_CODE = PRAM_RACE_CODE
    ORDER BY UMABAN;"
  end

  def getTanshoOdds(racecode)
    @my = Mysql::new("210.150.110.214","marusasu_test","marusasu","marusasu_jra")

    sqlString = @getTansoOddsString.clone
    sqlString = sqlString.sub(/PRAM_RACE_CODE/, "#{racecode}")
    @a = @my.query(sqlString)
    @my.close
    return @a
  end

  def getRaceDataYosou(todayDate)
    @my = Mysql::new("210.150.110.214","marusasu_test","marusasu","marusasu_jra")
      sqlString = @getRaceDataString.clone
      sqlString = sqlString.sub(/PARAM_KUBUN/, "2")

      sqlString = sqlString.sub(/PARAM_DATE/, "#{todayDate}")
      @a = @my.query(sqlString)
      @my.close
      return @a
  end
  def getUmaData(kettoBango)
    @my = Mysql::new("210.150.110.214","marusasu_test","marusasu","marusasu_jra")
    sqlString = @getUmaDataString.clone
    sqlString = sqlString.sub(/PARAM_KETTOBANGO/, "#{kettoBango}")
    sqlString = sqlString.sub(/PARAM_KUBUN/, "7") #成績（月曜）
    @a = @my.query(sqlString)
    @my.close
    return @a
  end
end

#1レースを保持するクラス　全ての馬毎（馬番）の情報を保持する16頭立てなら16レコード
class RaceComponent
  @@raceDB = RaceDB.new
  @@mukouTimeData = 999999

  def initialize
    @component = []
    @kaisaiKeibajoAndKyori = nil
    @kyoriOnly = nil
  end

  def getRankKeibajoAndKyori
    if @kyoriOnly[0][0] == nil or @kaisaiKeibajoAndKyori[0][0] == @mukouTimeData
      return nil
    end
    return @kaisaiKeibajoAndKyori
  end

  def getRankKyoriNomi
    if @kyoriOnly[0][0] == nil or @kyoriOnly[0][0] == @mukouTimeData
      return nil
    end
    return @kyoriOnly
  end

  def setRankTime
    @kaisaiKeibajoAndKyori = []
    @gaitoKeibajo = @component[0][1]
    @gaitoKyori = @component[0][2]
    @gaitoTrack = @component[0][4]

    puts "gaitoKeibajo"+@gaitoKeibajo.to_s
    puts "gaitoKyori"+@gaitoKyori.to_s
    puts "track" +@gaitoTrack.to_s
    @component.each do |com|
      puts "-----1-----------"
      @time = @@mukouTimeData
      com[5].each do |umadata|
        puts umadata[:race_code]
        puts umadata[:date]
        puts "#{umadata[:kyori]} gaitokyo  #{@gaitoKyori}"
        puts "#{umadata[:keibajo]} gaitokei #{@gaitoKeibajo}"
        puts umadata[:sohaTime]
        if umadata[:sohaTime].to_i < 1 then
          next  #取り消し等でゴールしていない場合に0となっているため除外が必要
        end

        if nil == @time
          puts "nil dayo"
          exit
        end

        if umadata[:keibajo] == @gaitoKeibajo and umadata[:kyori] == @gaitoKyori then
          if !((umadata[:track].to_i<=22) ^ (@gaitoTrack.to_i<=22)) and  umadata[:sohaTime].to_f < @time then
            @time = umadata[:sohaTime].to_f
            puts "@@@"
            puts @time
          end
        end
      end
      @kaisaiKeibajoAndKyori << [@time, com[0]]
    end

    @kaisaiKeibajoAndKyori.each do |t,u|
      puts "#{t} #{u}"
    end
    puts "------keibajogentei----------"
    #数値が低い順にソートする。
    @kaisaiKeibajoAndKyori = @kaisaiKeibajoAndKyori.sort { |a, b| a[0] <=> b[0] }
    @kaisaiKeibajoAndKyori.each do |t,u|
      puts "#{t} #{u}"
    end

    #距離のみの比較を行う　mada 当該競馬場とリファクタリングが必要
    @kyoriOnly = []
    @component.each do |com|
      puts "-----2-----------"
      @time = @@mukouTimeData
      com[5].each do |umadata|
        if umadata[:sohaTime].to_i < 1 then
          puts umadata[:sohaTime]
          puts "skip"
          next  #取り消し等でゴールしていない場合に0となっているため除外が必要
        end
        puts @gaitoKyori
        puts umadata[:kyori]
        if umadata[:kyori] == @gaitoKyori and !((umadata[:track].to_i<=22) ^ (@gaitoTrack.to_i<=22))  then
          if umadata[:sohaTime] != nil  and umadata[:sohaTime].to_f < @time then
            puts "-----@-----------"
            @time = umadata[:sohaTime].to_f
            puts @time
          end
        end
        puts "&&&"
        puts com[0]
        puts @time
        puts "&&&"
      end
      @kyoriOnly << [@time, com[0]]
    end

    @kyoriOnly.each do |t,u|
      puts "#{t} #{u}"
    end
    puts "-------kyorinomi---------"
    #数値が低い順にソートする。
    @kyoriOnly = @kyoriOnly.sort { |a, b| a[0] <=> b[0] }
    @kyoriOnly.each do |t,u|
      puts "#{t} #{u}"
    end
    return @kaisaiKeibajoAndKyori, @kyoriOnly
  end

  def setRace(**r)
    @umaData = []
    @umaDataOne = Hash.new
    @@raceDB.getUmaData(r[:kettoNo]).each do |race_code ,date, keibajo, sohaTime, kyori, trackCode, tanshoOdds|
      puts "#{date} odds #{tanshoOdds}"
      @umaDataOne = {:race_code=>race_code, :date=>date, :keibajo=>keibajo,
                    :sohaTime=>sohaTime, :kyori=>kyori, :tarack=>trackCode, :tanshoOdds=>tanshoOdds}
      @umaData << @umaDataOne
    end

    @newOdds = nil

    @component << [r[:umaban], r[:keibajo], r[:kyori], r[:bamei],r[:track], @umaData]
    printf(".")
  end

  def getRace
    return @component
  end
end

=begin
r = RaceDB.new
todayDate =  "BETWEEN '2020-04-23 00:00:00' AND '2020-04-27 00:00:00'\n"
a = r.getRaceDataYosou(todayDate)
aRaces = []

raceCode = nil
h = RaceComponent.new
a.each do  |race_code ,date, keibajo, raceNo, kyori, trackCode, umaban, kettoNo, bamei|
  if raceCode == nil then
    raceCode = race_code
  elsif raceCode != race_code then
    raceCode = race_code
    #開催競馬場かつ、施行距離毎の順位を決める。該当なしの場合はnil
    puts "keibajo#{keibajo}, raceNo#{raceNo}"
    h.setRankTime()
    aRaces << h
    h = RaceComponent.new
  end
  h.setRace(:race_code=>race_code, :date=>date, :keibajo=>keibajo, :raceNo=>raceNo, :kyori=>kyori,
            :track=>trackCode, :umaban=>umaban, :kettoNo=>kettoNo, :bamei=>bamei)
end
#開催競馬場かつ、施行距離毎の順位を決める。該当なしの場合はnil
h.setRankTime()
aRaces << h


aRaces.each do |race|
  puts "keibajo #{race.getRace[0][1]} kyori #{race.getRace[0][2]}"
  race.getRace.each do |comp|
    puts "umaban #{comp[0]} #{comp[3]}"
  end
  puts race.getRankKeibajoAndKyori()
end
=end
