require './race_db'
class Screening
  @raceDB = nil

  def initialize(filepath)
    @raceDB = RaceDB.new

    if filepath.nil?
      todayDate =  "BETWEEN '2020-04-23 00:00:00' AND '2020-04-27 00:00:00'\n"
      @yosou = @raceDB.getRaceDataYosou(todayDate)
      @aRaces = []

      @raceCode = nil
      @rComponent = RaceComponent.new
      @yosou.each do  |race_code ,date, keibajo, raceNo, kyori, trackCode, umaban, kettoNo, bamei|
        @trackCode = trackCode
        if @raceCode == nil then
          @raceCode = race_code
        elsif @raceCode != race_code then
          #開催競馬場かつ、施行距離毎の順位を決める。該当なしの場合はnil
          puts "keibajo#{keibajo}, raceNo#{raceNo}"
          @aRank = @rComponent.setRankTime()
          @aRaces << [@rComponent, @raceCode, trackCode, {:keibajoAndKyori=>@aRank[0], :kyori=>@aRank[1]}]
          @rComponent = RaceComponent.new

          @raceCode = race_code
        end
        @rComponent.setRace(:race_code=>race_code, :date=>date, :keibajo=>keibajo, :raceNo=>raceNo, :kyori=>kyori,
                             :track=>trackCode, :umaban=>umaban, :kettoNo=>kettoNo, :bamei=>bamei)
      end
      #開催競馬場かつ、施行距離毎の順位を決める。該当なしの場合はnil
      @aRank = @rComponent.setRankTime()
      @aRaces << [@rComponent, @raceCode, @trackCode, {:keibajoAndKyori=>@aRank[0], :kyori=>@aRank[1]}]

    else
      @b = File.open(filepath, "rb")
      if @b.nil?
        puts "error"
        exit
      else
        @aRaces = Marshal.load(@b)
        return @aRaces
      end
    end
  end

  def getRace
    return @aRaces
  end

  def getTanshoOdds(rececode)
    return @raceDB.getTanshoOdds(rececode)
  end

  def serialize(filepath)
    @d = Marshal.dump(@aRaces)
    File.open(filepath,"wb") do |file|
      file.write @d
    end
  end
end

=begin
s = Screening.new(nil)
s.getRace().each do |race|
  puts "keibajo #{race[0].getRace[0][1]} kyori #{race[0].getRace[0][2]}"
  race[0].getRace.each do |comp|
    puts "umaban #{comp[0]} #{comp[3]}"
  end
  puts race[1]
  #レース前オッズは馬毎レース情報には格納されない。
  #オッズ票から取得が必要。平場は前日提供されていないその際はnil
  s.getTanshoOdds(race[1]).each do |umaban, odds|
    puts odds
  end
end
s.serialize("scout.txt")
exit
=end


s = Screening.new("scout.txt")



#ここからがスクリーニング
#まず,1番人気馬を抽出する。同率オッズの場合は内枠有利として若い番号を取得
screeningResult = []
s.getRace().each do |race|
  oddsAndUmaban = []
  s.getTanshoOdds(race[1]).each do |umaban, odds|
    oddsAndUmaban << [odds, umaban]
  end
  #一番人気馬を求める
  if oddsAndUmaban != nil then
    oddsAndUmaban = oddsAndUmaban.sort { |a, b| a[0].to_f <=> b[0].to_f }#まさかのオッズが文字列。。。
    #oddsが平場のため入っていない場合？の処理
    if oddsAndUmaban[0] == nil then
      next
    end

    topNinkiUmaban = oddsAndUmaban[0][1]
    puts "topNiniki"
    puts topNinkiUmaban
  end

  #一番人気馬番の持ち時計順位を取得する
  puts "kAndK"
  puts race[3][:keibajoAndKyori]
  puts "kyori"
  puts race[3][:kyori]
  index = 1
  race[3][:keibajoAndKyori].each do |time, umaban|
    if topNinkiUmaban == umaban then
      if 999999 == time then
        index = -1
        puts "mukoudata1"
      end

      break
    end
    index += 1
  end
  keibajoAndKyoriJuni = index

  index = 1
  race[3][:kyori].each do |time, umaban|
    if topNinkiUmaban == umaban then
      if 999999 == time then
        index = -1
        puts "mukoudata2"
      end

      break
    end
    index += 1
  end
  kyoriJuni = index

  puts "kandk #{keibajoAndKyoriJuni}   kyori #{kyoriJuni}"

  #1番人気馬がどちらか4位以内の場合は荒れないレースとし除外する
  if (-1 == keibajoAndKyoriJuni and -1 == kyoriJuni) or keibajoAndKyoriJuni <= 4 or kyoriJuni <= 4 then
    puts "@@@@@@@@@@jogai&&&&&&&&&"

    next
  end

  # 荒れるレースとして、相手探しを行う。
  # 開催競馬場かつ当該距離で過去最高タイムで優秀な馬 3頭ピックアップする
  # 条件に適合しない場合もあるので注意　1頭もいないなど
  # まずは開催競馬場の重みづけをせず等価としてみる
  kouho = []
  index = 0
  race[3][:keibajoAndKyori].each do |time, umaban|
    if time == 999999 then
      break
    end
    kouho << umaban
  end

  race[3][:kyori].each do |time, umaban|
    if time == 999999 then
      break
    end
    kouho << umaban
  end

  if kouho.length < 1
    next
  end

  #同一馬番を除外
  kouho.uniq!
  #4頭に絞る
  kouho = kouho[0,4]

  puts "kouho"
  puts kouho
  puts '_'

  screeningResult << [kouho, race ]
end



screeningResult.each do |kouho, race|
  puts "racecpde #{race[1]} keibajo #{race[0].getRace[0][1]} kyori #{race[0].getRace[0][2]}"
  puts kouho
  puts
  puts "---------------------------------"
end








