require 'mysql'

class RaceData
  Raw = Struct.new(
      :rRaceCode,  :rKeibajoCode,  :rDate,
      :rRaceNo,    :rTorokusu,
      :rKyosoShubetsu,     :rKyosoKigo,
      :rKyosoJoken2, :rKyosoJoken3,  :rKyosoJoken4, :rKyosoJoken5,
      :rKyori,:rTrack
  )

  def initialize(order)
    @db = []
    case order
      when 0 then
        my = Mysql::new("210.150.110.214","marusasu_test","marusasu","marusasu_jra")
        a = my.query("select\n
          RACE_CODE,            KEIBAJO_CODE,               KAISAI_NENGAPPI,       RACE_BANGO, TOROKU_TOSU,\n
          KYOSO_SHUBETSU_CODE,  KYOSO_KIGO_CODE,            KYOSO_JOKEN_CODE_2SAI, KYOSO_JOKEN_CODE_3SAI,\n
          KYOSO_JOKEN_CODE_4SAI,KYOSO_JOKEN_CODE_5SAI_IJO,  KYORI,                 TRACK_CODE\n
          from JVD_RACE_SHOSAI Order by RACE_CODE ASC").each do  |rc,kc,date,rn,tt,ksc,kkc,kj2,kj3,kj4,kj5,kyori,track|
          puts "#{rc},#{kc},#{date},#{rn},#{tt},#{ksc},#{kkc},#{kj2},#{kj3},#{kj4},#{kj5},#{kyori},#{track}"
          @db << Raw.new(rc,kc,date,rn,tt,ksc,kkc,kj2,kj3,kj4,kj5,kyori,track)
        end
        my.close
        serialize
      when 1 then
        @b = File.open("outr.txt","rb")
        @db = Marshal.load(@b)
        #puts @db
      else
        puts "error"
        return @db
    end
  end

  def serialize
    @d = Marshal.dump(@db)
    File.open("outr.txt","wb") do |file|
      file.write @d
    end
  end
  def getSearchOrder(order)
    @s = []
    @db.each do |db|
      if db[order[0]] == order[1]
        #puts db[order[0]]
        @s << db
      end
    end
    return @s
  end
end

#RaceData.new(0)