require 'mysql'

my = Mysql::new("210.150.110.214","marusasu_test","marusasu","marusasu_jra")
a = my.query("
SELECT JVD_RACE_SHOSAI.RACE_CODE, JVD_RACE_SHOSAI.KAISAI_NENGAPPI,\n
       JVD_RACE_SHOSAI.KEIBAJO_CODE, JVD_RACE_SHOSAI.RACE_BANGO, JVD_UMAGOTO_RACE_JOHO.UMABAN,\n
       JVD_UMAGOTO_RACE_JOHO.KETTO_TOROKU_BANGO, JVD_UMAGOTO_RACE_JOHO.BAMEI\n
FROM marusasu_jra.JVD_RACE_SHOSAI LEFT JOIN marusasu_jra.JVD_UMAGOTO_RACE_JOHO\n
     ON JVD_RACE_SHOSAI.RACE_CODE = JVD_UMAGOTO_RACE_JOHO.RACE_CODE
WHERE JVD_RACE_SHOSAI.DATA_KUBUN = '2'\n
AND (JVD_RACE_SHOSAI.KYOSO_SHUBETSU_CODE = 13 OR JVD_RACE_SHOSAI.KYOSO_SHUBETSU_CODE = 14)\n
AND (JVD_RACE_SHOSAI.KYOSO_JOKEN_CODE_SAIJAKUNEN = '016'\n
     OR JVD_RACE_SHOSAI.KYOSO_JOKEN_CODE_SAIJAKUNEN = '999')\n
ORDER BY JVD_RACE_SHOSAI.KAISAI_NENGAPPI;")

#取得したデータをハッシュで格納し、それを配列化しておく。
aRace = []
a.each do  |race_code ,date, keibajo, raceNo, umaban, kettoNo, bamei|
  h = {:race_code=>race_code, :date=>date, :keibajo=>keibajo, :raceNo=>raceNo, :umaban=>umaban,
       :kettoNo=>kettoNo, :bamei=>bamei}
  aRace << h
end

aRace.each do |a|
  puts a[:bamei]
end


#馬名毎に情報を取得する。
kettoBango = '2016104514'
a2 = my.query("
SELECT JVD_UMAGOTO_RACE_JOHO.RACE_CODE, JVD_UMAGOTO_RACE_JOHO.KAISAI_NENGAPPI,\n
       JVD_UMAGOTO_RACE_JOHO.KEIBAJO_CODE, MIN(JVD_UMAGOTO_RACE_JOHO.SOHA_TIME),\n
       JVD_RACE_SHOSAI.KYORI\n
FROM marusasu_jra.JVD_UMAGOTO_RACE_JOHO LEFT JOIN marusasu_jra.JVD_RACE_SHOSAI\n
ON JVD_UMAGOTO_RACE_JOHO.RACE_CODE = JVD_RACE_SHOSAI.RACE_CODE\n
WHERE JVD_UMAGOTO_RACE_JOHO.DATA_KUBUN = '7' AND KETTO_TOROKU_BANGO = #{kettoBango}\n
GROUP BY JVD_UMAGOTO_RACE_JOHO.KEIBAJO_CODE, JVD_RACE_SHOSAI.KYORI;")

aaa = []
a2.each do  |race_code ,date, keibajo, minTime, kyori|
  h = {:race_code=>race_code, :date=>date, :keibajo=>keibajo, :minTime=>minTime, :kyori=>kyori}
  aaa << h
end

aaa.each do |bbb|
  puts bbb[:minTime]
end

my.close